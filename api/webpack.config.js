const path = require("path");
const nodeExternals = require('webpack-node-externals');
const WebpackShellPlugin = require('webpack-shell-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const {
  NODE_ENV = 'production',
} = process.env;
const isDevelopment = process.env.NODE_ENV === 'development';
module.exports = {
  entry: "./src/index.js",
  output: {
    path: path.resolve(process.cwd(), 'build'),
    filename: 'index.js'
  },
  externals: [nodeExternals()],
  target: 'node',
  optimization: {
    minimizer: [new UglifyJsPlugin()],
  },
  mode: NODE_ENV,
  watch: isDevelopment,
  module: {
    rules: [{
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new CopyPlugin([{
        from: path.resolve(__dirname, 'static'),
        to: path.resolve(__dirname, 'build/static')
      },
    ]),
    new WebpackShellPlugin({
      onBuildEnd: isDevelopment ? ['yarn dev'] : []
    })
  ],
};
