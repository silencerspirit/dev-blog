const mongoose = require('mongoose');
mongoose.Promise = require('bluebird')
mongoose.connect('mongodb://localhost/devblogdatabase', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true
});
const database = mongoose.connection;
database.on('error', console.error.bind(console, 'connection error:'));
database.once('open', function callback() {
  console.log("MongoDB Connected...");
});

module.exports = database