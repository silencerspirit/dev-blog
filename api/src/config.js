const jwt = require('jsonwebtoken');
const authSecret = 'silencerspirit'


exports.authSecret = authSecret,
exports.isAuthenticated = function (request, response, next) {
  const token = request.headers.authorization
  if (token) {
    // verifies secret and checks if the token is expired
    jwt.verify(token, authSecret, function (error) {
      if (error) {
        return response.status(401).json({
          message: 'unauthorized'
        })
      } else {
        return next();
      }
    });
  } else {
    return response.status(401).json({
      message: 'unauthorized'
    })
  }
}