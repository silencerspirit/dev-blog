exports.baseErrors = (error, item, response) => {
  if (error) {
    return response.status(500).json({
      message: 'E_ERROR'
    })
  }
  if (!item) {
    return response.status(404).json({
      message: 'E_NOT_FOUND_IN_BASE'
    })
  }
}