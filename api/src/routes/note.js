const { Router } = require('express')
const {isAuthenticated} = require('#root/config')
const noteConroller = require('#controllers/note-controller')
const router = Router()

router.post('/note/save', isAuthenticated, noteConroller.saveNote)
router.put('/note/update', isAuthenticated, noteConroller.updateNote)
router.get('/note/get', isAuthenticated, noteConroller.getNote)

module.exports = router
