const { Router } = require('express')
const authController = require('#controllers/auth-controller')
const router = Router()
const {
  fillFieldsValidation,
  getValidationResults
} = require('#root/validations')

router.post(
  '/admin/login',
  fillFieldsValidation(['email', 'password']),
  getValidationResults,
  authController.adminLogin
)

router.post(
  '/admin/register',
  fillFieldsValidation(['name', 'email', 'password']),
  getValidationResults,
  authController.adminRegistry
)

router.post(
  '/admin/token/test',
  fillFieldsValidation(['token']),
  getValidationResults,
  authController.adminTokenTest
)
module.exports = router
