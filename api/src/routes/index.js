const path = '/api';

module.exports = function(app) {
  app.use(path, require('#routes/admin'));
  app.use(path, require('#routes/note'));
  app.use(path, require('#routes/article'));
  app.use(require('#routes/upload'));
}