const { Router } = require('express')
const { isAuthenticated } = require('#root/config')
const fs = require('fs')
const router = Router()
router.post(
  '/upload',
  isAuthenticated,
  (request, response) => {
    const { file } = request.files;
    file.mv( `static/images/${file.name}`);
    return response.json({
      link: `images/${file.name}`
    })
  }
)

router.get('/upload/get', isAuthenticated, (request, response) => {
  const filesArray = []
  fs.readdirSync('static/images/').forEach(file => {
    filesArray.push(file)
  });
  return response.json({
    files: filesArray
  })
})

router.delete('/upload/delete', isAuthenticated, (request, response) => {
  const {name} = request.body;
  fs.unlinkSync(`static/images/${name}`);
  return response.json({
    message: 'SUCCESS'
  })
})
module.exports = router
