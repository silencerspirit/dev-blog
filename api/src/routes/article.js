const { Router } = require('express')
const {isAuthenticated} = require('#root/config')
const articleController = require('#controllers/article-controller')
const router = Router()

router.post('/article/create', isAuthenticated, articleController.createArticle)
router.post('/article/list', isAuthenticated, articleController.getArticles)
router.post('/article/get', isAuthenticated, articleController.getArticle)
router.put('/article/visibility', isAuthenticated, articleController.toggleVisibility)
router.put('/article/edit', isAuthenticated, articleController.editArticle)
router.delete('/article/delete', isAuthenticated, articleController.deleteArticle)
// router.put('/note/update', isAuthenticated, noteConroller.updateNote)
// router.get('/note/get', isAuthenticated, noteConroller.getNote)

module.exports = router
