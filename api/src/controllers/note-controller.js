const Note = require('#models/note')
const { v4: uuidv4 } = require('uuid');
const { baseErrors } = require('#root/utils')

const saveNote = (request, response) => {
  const note = new Note({
    text: request.body.text,
    id: uuidv4(),
  })

  note.save((error, item) => {
    if (error || !item) return baseErrors(error, item, response)
    return response.json({
      message: 'SUCCESS',
      id: item.id
    })
  })
}

const getNote = (request, response) => {
  Note.find({}, (error, items) => {
    if (error || !items) return baseErrors(error, items, response)
    const [item] = items
    return response.json({
      item,
      message: 'SUCCESS'
    })
  })
}

const updateNote = (request, response) => {
  const id = request.body.id;
  const text = request.body.text;
  Note.findOneAndUpdate({id}, {text}, {new: true}, (error) => {
    if (error) {
      return response.status(500).json({
        message: 'E_ERROR',
      });
    }
    return response.json({
      message: 'SUCCESS'
    })
  })
}

module.exports = {
  saveNote,
  getNote,
  updateNote
}