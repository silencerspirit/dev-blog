const Admin = require('#models/admin')
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const { authSecret } = require('#root/config')
const { baseErrors } = require('#root/utils')

const adminRegistry = (request, response) => {
  const admin = new Admin({
    name: request.body.name,
    email: request.body.email,
    password: request.body.password,
  })

  const salt = bcrypt.genSaltSync(10);
  const hash = bcrypt.hashSync(admin.password, salt);
  admin.password = hash

  admin.save((error, item) => {
    if (error || !item) return baseErrors(error, item, response)
    return response.json({
      message: 'SUCCESS',
      id: item._id
    })
  })
}


const adminLogin = (request, response) => {
  Admin.findOne({email: request.body.email}, (error, item) => {
    if (error || !item) return baseErrors(error, item, response)

    return bcrypt.compare(request.body.password, item.password, async (error_, isMatch) => {
      if(isMatch) {
        const token = jwt.sign({
          id: item._id,
          email: item.email,
          date: new Date().toJSON(),
        }, authSecret)
        await Admin.findByIdAndUpdate(item._id, {token})
        return response.json({
          token
        })
      }
      if(error_) {
        return response.status(500).json({
          message: 'E_AUTH_ERROR'
        })
      }
    })
  })
}

const adminTokenTest = (request, response) => {
  Admin.findOne({token: request.body.token}, (error, item) => {
    if (error || !item) return baseErrors(error, item, response)
    return response.json({
      message: 'SUCCESS'
    })
  })
}

module.exports = {
  adminRegistry,
  adminLogin,
  adminTokenTest,
}