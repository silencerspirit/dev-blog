const Article = require('#models/article')
const { v4: uuidv4 } = require('uuid');
const { baseErrors } = require('#root/utils')

const createArticle = (request, response) => {
  const {
    published,
    title,
    synopsis,
    text,
    date,
    tag,
    thumbnail
  } = request.body
  const article = new Article({
    published,
    thumbnail,
    title,
    synopsis,
    text,
    date,
    tag,
    id: uuidv4(),
  })

  article.save((error, item) => {
    if (error || !item) return baseErrors(error, item, response)
    return response.json({
      message: 'SUCCESS',
      id: item.id
    })
  })
}

const getArticles = async (request, response) => {
  const page = Number.parseInt(request.body.page);
  const limit = Number.parseInt(request.body.limit);

  const startIndex = (page - 1) * limit;
  const endIndex = page * limit;

  const results = {}

  if (endIndex < await Article.countDocuments().exec()) {
    results.next = {
      page: page + 1,
      limit: limit
    }
  }

  if (startIndex > 0) {
    results.previous = {
      page: page - 1,
      limit: limit
    }
  }
  try {
   results.articles = await Article.find().limit(limit).skip(startIndex).exec();
   const json = { ...results };
   return response.json(json)
  } catch (error) {
    return response.status(500).json({
      message: 'E_ERROR'
    })
  }
}

const getArticle = (request, response) => {
  const id = request.body.id;

  Article.findOne({id}, (error, item) => {
    if (error || !item) return baseErrors(error, item, response)
    return response.json({
      item
    })
  })
}

const toggleVisibility = (request, response) => {
  const {id, published} = request.body;
  Article.findOneAndUpdate({ id }, { published} , { new: true }, (error, item) => {
    if (error || !item) return baseErrors(error, item, response)
    return response.json({
      message: 'SUCCESS',
    })
  })
}

const deleteArticle = (request, response) => {
  const {id} = request.body;
  Article.findOneAndRemove({id}, (error, item) => {
    if(error || !item) return  baseErrors(error, item, response)
    return response.json({
      message: 'SUCCESS',
    })
  })
}

const editArticle = (request, response) => {
  const {id} = request.body;
  Article.findOneAndUpdate({ id }, {...request.body} , { new: true }, (error, item) => {
    if (error || !item) return baseErrors(error, item, response)
    return response.json({
      message: 'SUCCESS',
    })
  })
}

module.exports = {
  createArticle,
  deleteArticle,
  getArticle,
  getArticles,
  toggleVisibility,
  editArticle
}