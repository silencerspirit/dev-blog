import 'regenerator-runtime/runtime';
const express = require('express');
const cors = require('cors');
const fileUpload = require('express-fileupload');
// eslint-disable-next-line no-unused-vars
const database = require('./database')
const helmet = require('helmet');
const routes = require('#routes')
// Create express instnace
const app = express()
app.use(express.static('static'));
app.use(helmet());
app.use(fileUpload({
  createParentPath: true
}));
app.use(cors());
app.options('*', cors());
app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));

routes(app)

// Export the server middleware
app.listen(3001, () => {
  console.log('server started')
})