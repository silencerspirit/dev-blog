const {body, validationResult} = require('express-validator')

exports.getValidationResults = (request, response, next) => {
  const errors = validationResult(request);
  if (!errors.isEmpty()) {
    return response.status(422).json({
      errors: errors.mapped(),
      message: 'E_INCORRECT_FIELDS'
    });
  }
  next()
}
exports.fillFieldsValidation = fields => fields.map(field => body(field).isLength({min: 1}))