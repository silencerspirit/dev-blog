const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Note = new Schema ({
  text: { type: String, required: true },
  id: { type: String, required: true },
});

module.exports  = mongoose.model('Note', Note)