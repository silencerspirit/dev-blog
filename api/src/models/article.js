const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Article = new Schema ({
  published: { type: Boolean, required: true, default: false },
  title: { type: String, required: true, unique: true},
  synopsis: { type: String, required: true },
  text: { type: String, required: true},
  date: { type: String, required: true},
  tag: { type: String, required: true},
  thumbnail: { type: String, required: true},
  comments: { type: Array },
  id: { type: String, required: true },
});

module.exports = mongoose.model('Article', Article)