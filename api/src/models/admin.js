const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Admin = new Schema ({
  name: { type: String, required: true, index: { unique: true } },
  email: { type: String, required: true, index: { unique: true } },
  password: { type: String, required: true },
  token: { type: String, index: { unique: true } },
});

module.exports  = mongoose.model('Admin', Admin)