import Vue from 'vue'
export default () => {
  Vue.component('ElButton', () =>
    import(/* webpackChunkName: 'el-button' */ 'element-ui/lib/button')
  )
}
