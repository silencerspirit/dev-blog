import { setField } from "@/store/helpers/";
export default {
  state: () => ({
    articlesInfo: {},
    currentItem: {}
  }),
  getters: {},
  actions: {
    async articleHandler({ dispatch }, { isEdit, ...json }) {
      const topic = `article/${isEdit ? "edit" : "create"}`;
      try {
        await dispatch(
          "httpController",
          { topic, json, type: isEdit ? "put" : "post" },
          { root: true }
        );
        dispatch(
          "notify",
          {
            type: "success",
            title: "Сохранено",
            message: `Статья ${isEdit ? "изменена" : "создана"}`
          },
          { root: true }
        );
      } catch (error) {
        console.error(`Ошибка в запросе ${topic}`, error);
      }
    },

    async getArticles({ commit, dispatch }) {
      const topic = "article/list";
      const json = { page: 1, limit: 10 };
      try {
        const value = await dispatch(
          "httpController",
          { topic, json, type: "post" },
          { root: true }
        );
        commit("setField", { field: "articlesInfo", value });
      } catch (error) {
        console.error(`Ошибка в запросе ${topic}`, error);
      }
    },

    async toggleVisibility({ dispatch }, json) {
      const topic = "article/visibility";

      try {
        await dispatch(
          "httpController",
          { topic, json, type: "put" },
          { root: true }
        );
        dispatch(
          "notify",
          {
            type: `${json.published ? "success" : "warning"}`,
            title: "Сохранено",
            message: `Статья ${
              json.published ? "опубликавана" : "снята с публикации"
            }`
          },
          { root: true }
        );
        dispatch("getArticles");
      } catch (error) {
        console.error(`Ошибка в запросе ${topic}`, error);
      }
    },

    getCurrentItem({ commit }, value) {
      commit("setField", { field: "currentItem", value });
    }
  },
  mutations: {
    setField
  },
  namespaced: true
};
