import { setField } from "@/store/helpers/";
export default {
  getters: {},
  actions: {
    async saveNote({ dispatch }, { text }) {
      const topic = "note/save";
      const json = {
        text
      };

      try {
        await dispatch(
          "httpController",
          { topic, json, type: "post" },
          { root: true }
        );
      } catch (error) {
        console.error(`Ошибка в запросе ${topic}`, error);
      }
    },
    async updateNote({ dispatch }, { text, id }) {
      const topic = "note/update";
      const json = {
        text,
        id
      };

      try {
        await dispatch(
          "httpController",
          { topic, json, type: "put" },
          { root: true }
        );
      } catch (error) {
        console.error(`Ошибка в запросе ${topic}`, error);
      }
    },
    async getNote({ dispatch }) {
      const topic = "note/get";
      try {
        const { item } = await dispatch(
          "httpController",
          { topic },
          { root: true }
        );
        return item;
      } catch (error) {
        console.error(`Ошибка в запросе ${topic}`, error);
      }
    }
  },
  mutations: {
    setField
  },
  namespaced: true
};
