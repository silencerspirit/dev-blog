export function setField(state, { field, value }) {
  if (!field) return;
  state[field] = value;
}
