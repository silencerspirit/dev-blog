import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import { setField } from "@/store/helpers/";
import { Notification } from "element-ui";
// modules
import note from "./note";
import article from "./article";

Vue.use(Vuex);
export default new Vuex.Store({
  state: {
    token: localStorage.getItem("token") || null,
    isAuthenticated: false,
    serverURL: process.env.VUE_APP_SERVER_URL
  },
  mutations: {
    setField
  },
  actions: {
    async httpController({ state }, { topic, json = {}, type }) {
      const url = `${state.serverURL}/api/${topic}`;
      if (state.token && state.isAuthenticated) {
        axios.defaults.headers.common["Authorization"] = state.token;
      }
      try {
        if (["post", "put", "delete"].includes(type)) {
          console.log(
            { log: "REQUEST", time: new Date().toJSON() },
            { url, json }
          );
          const isDeleteMethod = type === "delete";
          const { data } = isDeleteMethod
            ? await axios[type](url, {
                data: json
              })
            : await axios[type](url, json);
          console.log(
            { log: "RESPONSE", time: new Date().toJSON() },
            { url, data }
          );
          return data;
        }
        console.log({ log: "REQUEST", time: new Date().toJSON() }, { url });
        const { data } = await axios.get(url);
        console.log(
          { log: "RESPONSE", time: new Date().toJSON() },
          { url, data }
        );
        return data;
      } catch (error) {
        console.error(
          { log: "ERROR", time: new Date().toJSON() },
          {
            url,
            error: error.response.data.message
          }
        );
        throw new Error(error.response.data.message);
      }
    },

    // eslint-disable-next-line no-empty-pattern
    notify({}, { type, title, message, duration = 2000 }) {
      Notification({
        type,
        title,
        message,
        duration
      });
    },

    async tokenCompareTest({ commit, state }, token) {
      const topic = "admin/token/test";
      const url = `${state.serverURL}/api/${topic}`;
      try {
        await axios.post(url, { token });
        commit("setField", { field: "isAuthenticated", value: true });
      } catch (error) {
        console.error(error);
      }
    },

    logout({ commit }) {
      localStorage.removeItem("token");
      commit("setField", { field: "token", value: "" });
      commit("setField", { field: "isAuthenticated", value: false });
    }
  },
  modules: {
    note,
    article
  }
});
