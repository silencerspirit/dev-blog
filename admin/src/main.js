import Vue from "vue";
import App from "./App.vue";
import {
  Button,
  Form,
  FormItem,
  Input,
  Menu,
  Submenu,
  MenuItemGroup,
  MenuItem,
  Dialog,
  Switch,
  Notification,
  Upload
} from "element-ui";
import "element-theme-dark";
import "@/assets/scss/main.scss";
import router from "./router";
import store from "./store";
[
  Button,
  Form,
  FormItem,
  Input,
  Menu,
  Submenu,
  MenuItemGroup,
  MenuItem,
  Dialog,
  Switch,
  Upload
].forEach(component => Vue.use(component));
Vue.config.productionTip = false;
Vue.prototype.$notify = Notification;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
