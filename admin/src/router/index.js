import Vue from "vue";
import VueRouter from "vue-router";
import { createAuthGuard } from "./guards";
import store from "@/store";
import ROUTES from "@/router/utils/routes";
Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: ROUTES.HOME,
    component: () => import(/* webpackChunkName: "home" */ "@/views/Home"),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/articles",
    name: ROUTES.ARTICLES,
    component: () =>
      import(/* webpackChunkName: "articles" */ "@/views/Articles"),
    meta: {
      requiresAuth: true
    },
    redirect: { name: ROUTES.ARTICLES_LIST },
    children: [
      {
        path: "form",
        name: ROUTES.ARTICLES_ADD,
        component: () =>
          import(
            /* webpackChunkName: "articles" */
            "@/views/ArticlesViews/ArticlesForm"
          )
      },
      {
        path: "edit/:id",
        name: ROUTES.ARTICLES_EDIT,
        component: () =>
          import(
            /* webpackChunkName: "articles" */
            "@/views/ArticlesViews/ArticlesForm"
          )
      },
      {
        path: "list",
        name: ROUTES.ARTICLES_LIST,
        component: () =>
          import(
            /* webpackChunkName: "articles" */
            "@/views/ArticlesViews/ArticlesList"
          )
      }
    ]
  },
  {
    path: "/about",
    name: ROUTES.ABOUT,
    component: () => import(/* webpackChunkName: "about" */ "@/views/About"),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/photos",
    name: ROUTES.PHOTOS,
    component: () => import(/* webpackChunkName: "photos" */ "@/views/Photos"),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/login",
    name: ROUTES.LOGIN,
    component: () => import(/* webpackChunkName: "login" */ "@/views/Login")
  },
  {
    path: "*",
    redirect: store.state.isAuthenticated ? "/" : "/login"
  }
];
const router = new VueRouter({
  mode: "history",
  base: __dirname,
  routes
});
router.beforeEach(createAuthGuard({ store }));
export default router;
