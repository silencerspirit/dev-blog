export const createAuthGuard = ({ store }) => async (to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    const token = localStorage.getItem("token");
    if (token && !store.state.isAuthenticated) {
      try {
        await store.dispatch("tokenCompareTest", token);
        next();
      } catch (error) {
        next({
          path: "/login"
        });
      }
    } else if (!store.state.isAuthenticated) {
      next({
        path: "/login"
      });
    } else next();
  } else next();
};
