export default {
  HOME: "home",
  LOGIN: "login",
  ARTICLES: "articles",
  ARTICLES_ADD: "articles.form",
  ARTICLES_EDIT: "articles.edit",
  ARTICLES_LIST: "articles.list",
  ABOUT: "about",
  PHOTOS: "photos"
};
